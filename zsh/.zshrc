ZSH_DISABLE_COMPFIX="true"
ZSH="/home/amolith/.oh-my-zsh"
ZSH_THEME="agnoster"
DEFAULT_USER="amolith"
prompt_context(){}
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
export PATH="$PATH:$HOME/dotfiles/bin"
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$PATH:$HOME/.local/bin"
export PATH="$HOME/.gem/ruby/2.6.0/bin:$PATH"
export PATH="$HOME/.rbenv/bin:$PATH"
export GEM_HOME="$HOME/gems"
export CHROOT="$HOME/chroot"
export EDITOR="nvim"
export QT_QPA_PLATFORMTHEME="qt5ct"
export LC_TIME=en_GB.UTF-8
export MOZ_USE_XINPUT2=1

[[ -r "/usr/share/z/z.sh" ]] && source /usr/share/z/z.sh

# PLUGINS
plugins=(
  notify
  git
  zsh-autosuggestions
)


# STARTUP
(cat ~/.cache/wal/sequences &)
source $ZSH/oh-my-zsh.sh
source ~/.cache/wal/colors-tty.sh

# ALIASES
alias xclip="xclip -selection c"
alias ncdu="ncdu --color dark -rr -x --exclude .git --exclude node_modules"
alias y="strat -r arch yay"
alias b="bluetoothctl"
alias vim="nvim"
alias v="nvim"
alias ssh="ssh -Y"
unset zle_bracketed_paste

eval "$(rbenv init -)"
