[colors]
background = ${xrdb:color0:#222}
foreground = ${xrdb:color7:#222}
foreground-alt = ${xrdb:color7:#222}
primary = ${xrdb:color1:#222}
secondary = ${xrdb:color2:#222}
alert = ${xrdb:color3:#222}

[bar/top]
monitor = eDP-1
width = 100%
height = 20
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 0
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 1
module-margin-left = 1
module-margin-right = 2
font-0 = "Fira Code:size=8"
font-1 = "Font Awesome:size=9"
font-2 = Weather Icons:size=12;1
modules-left = i3
modules-center = date
modules-right = openweathermap-simple memory cpu network battery
tray-position = none
tray-padding = 2
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
cursor-click = pointer
cursor-scroll = ns-resize

[bar/bottom]
monitor = eDP-1
width = 100%
height = 20
bottom = true
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
font-0 = "Fira Code:size=8"
font-1 = "Font Awesome:size=9"
modules-center = mpd pulseaudio
cursor-click = pointer
cursor-scroll = ns-resize
tray-position = right
tray-padding = 0

[bar/top2]
monitor = HDMI-1
width = 100%
height = 20
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 0
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 1
module-margin-left = 1
module-margin-right = 2
font-0 = "Fira Code:size=8"
font-1 = "Font Awesome:size=9"
font-2 = Weather Icons:size=12;1
modules-left = i3
modules-center = date
modules-right = openweathermap-simple memory cpu network battery
tray-position = none
tray-padding = 2
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
cursor-click = pointer
cursor-scroll = ns-resize

[bar/bottom2]
monitor = HDMI-1
width = 100%
height = 20
bottom = true
fixed-center = true
background = ${colors.background}
foreground = ${colors.foreground}
line-size = 3
line-color = #f00
border-size = 0
border-color = #00000000
padding-left = 0
padding-right = 2
module-margin-left = 1
module-margin-right = 2
font-0 = "Fira Code:size=8"
font-1 = "Font Awesome:size=9"
modules-center = mpd pulseaudio
cursor-click = pointer
cursor-scroll = ns-resize
tray-position = right
tray-padding = 0

[module/filesystem]
type = internal/fs
interval = 25
mount-0 = /
label-mounted =  %{F#0a81f5}%mountpoint%%{F-}: %percentage_used%%
label-unmounted = %mountpoint% not mounted
label-unmounted-foreground = ${colors.foreground-alt}

[module/bspwm]
type = internal/bspwm

label-focused = %index%
label-focused-background = ${colors.primary}
;label-focused-underline= ${xrdb:color9:#222}
label-focused-padding = 2

label-occupied = %index%
label-occupied-padding = 2

label-urgent = %index%!
label-urgent-background = ${colors.alert}
label-urgent-padding = 2

label-empty = %index%
label-empty-foreground = ${colors.foreground-alt}
label-empty-padding = 2

ws-icon-0 = 1;
ws-icon-1 = 2;
ws-icon-2 = 3;
ws-icon-3 = 4;
ws-icon-4 = 5;
ws-icon-5 = 6;
ws-icon-6 = 7;
ws-icon-7 = 8;
ws-icon-8 = 9;
ws-icon-9 = 10;

; Separator in between workspaces
; label-separator = |

[module/i3]
type = internal/i3
format = <label-state> <label-mode>
index-sort = true
wrapping-scroll = false

; Only show workspaces on the same output as the bar
;pin-workspaces = true

label-mode-padding = 2
label-mode-foreground = #000
label-mode-background = ${colors.primary}

; focused = Active workspace on focused monitor
label-focused = %index%
label-focused-background = ${module/bspwm.label-focused-background}
;label-focused-underline = ${module/bspwm.label-focused-underline}
label-focused-padding = ${module/bspwm.label-focused-padding}

; unfocused = Inactive workspace on any monitor
label-unfocused = %index%
label-unfocused-padding = ${module/bspwm.label-occupied-padding}

; visible = Active workspace on unfocused monitor
label-visible = %index%
label-visible-background = ${self.label-focused-background}
;label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Workspace with urgency hint set
label-urgent = %index%
label-urgent-background = ${module/bspwm.label-urgent-background}
label-urgent-padding = ${module/bspwm.label-urgent-padding}

[module/mpd]
type = internal/mpd
format-online = <icon-prev>  <label-song>  <icon-next>

icon-prev = 
icon-play = 
icon-pause = 
icon-next = 

label-song-maxlen = 0
label-song-ellipsis = true

[module/cpu]
type = internal/cpu
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
;format-underline = #f90000
label = %percentage:2%%

[module/memory]
type = internal/memory
interval = 2
format-prefix = " "
format-prefix-foreground = ${colors.foreground-alt}
;format-underline = #4bffdc
label = %percentage_used%%

[module/date]
type = internal/date
interval = 60
date = %a %d %b
time = %H:%M
label =  %date%   %time%
format-padding = 1

[module/pulseaudio]
type = internal/pulseaudio
format-volume =   <ramp-volume> <label-volume>
format-muted = 0%  
ramp-volume-0 = 
ramp-volume-1 = 
ramp-volume-2 = 

[module/battery]
type = internal/battery
battery = BAT0
adapter = ACAD
full-at = 98
format-charging = <animation-charging> <label-charging>
;format-charging-underline = #ffb52a
format-discharging = <animation-discharging> <label-discharging>
;format-discharging-underline = ${self.format-charging-underline}
format-full-prefix = " "
format-full-prefix-foreground = ${colors.foreground-alt}
;format-full-underline = ${self.format-charging-underline}
ramp-charging-0 = 
ramp-charging-1 = 
ramp-charging-2 = 
ramp-charging-3 = 
ramp-charging-4 = 
ramp-capacity-foreground = ${colors.foreground-alt}
animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-foreground = ${colors.foreground-alt}
animation-charging-framerate = 750
animation-discharging-0 = 
animation-discharging-1 = 
animation-discharging-2 = 
animation-discharging-3 = 
animation-discharging-foreground = ${colors.foreground-alt}
animation-discharging-framerate = 750

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 150
format = <ramp> <label>
;format-underline = #f50a4d
format-warn = <ramp> <label-warn>
;format-warn-underline = ${self.format-underline}
label = %temperature-f%
label-warn = %temperature-f%
label-warn-foreground = ${colors.secondary}
ramp-0 = 
ramp-1 = 
ramp-2 = 
ramp-foreground = ${colors.foreground-alt}

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
[global/wm]
margin-top = 5
margin-bottom = 5
; vim:ft=dosini

[module/openweathermap-simple]
type = custom/script
exec = ~/.config/polybar/openweathermap-simple.sh
interval = 600
label-font = 3

[module/network]
type = internal/network
interface = wlo1
interval = 3.0
format-connected =<label-connected>
format-connected-prefix-foreground = ${colors.foreground-alt}
label-connected =
format-disconnected = <label-disconnected>
label-disconnected =
label-disconnected-foreground = ${colors.secondary}
format-padding = 1
