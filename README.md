
```
_____/\\\\\\\\\_______________________________________/\\\\\\_________________________/\\\__________/\\\\______________        
 ___/\\\\\\\\\\\\\____________________________________\////\\\________________________\/\\\_________\///\\______________       
  __/\\\/////////\\\______________________________________\/\\\_____/\\\_____/\\\______\/\\\__________/\\/_______________      
   _\/\\\_______\/\\\____/\\\\\__/\\\\\_______/\\\\\_______\/\\\____\///___/\\\\\\\\\\\_\/\\\_________\//_____/\\\\\\\\\\_     
    _\/\\\\\\\\\\\\\\\__/\\\///\\\\\///\\\___/\\\///\\\_____\/\\\_____/\\\_\////\\\////__\/\\\\\\\\\\_________\/\\\//////__    
     _\/\\\/////////\\\_\/\\\_\//\\\__\/\\\__/\\\__\//\\\____\/\\\____\/\\\____\/\\\______\/\\\/////\\\________\/\\\\\\\\\\_   
      _\/\\\_______\/\\\_\/\\\__\/\\\__\/\\\_\//\\\__/\\\_____\/\\\____\/\\\____\/\\\_/\\__\/\\\___\/\\\________\////////\\\_  
       _\/\\\_______\/\\\_\/\\\__\/\\\__\/\\\__\///\\\\\/____/\\\\\\\\\_\/\\\____\//\\\\\___\/\\\___\/\\\_________/\\\\\\\\\\_ 
        _\///________\///__\///___\///___\///_____\/////_____\/////////__\///______\/////____\///____\///_________\//////////__
__/\\\\\\\\\\\\_______________________________________/\\\\\________/\\\\\\________________________________                    
 _\/\\\////////\\\___________________________________/\\\///________\////\\\________________________________                   
  _\/\\\______\//\\\___________________/\\\__________/\\\_______/\\\____\/\\\________________________________                  
   _\/\\\_______\/\\\_____/\\\\\_____/\\\\\\\\\\\__/\\\\\\\\\___\///_____\/\\\________/\\\\\\\\___/\\\\\\\\\\_                 
    _\/\\\_______\/\\\___/\\\///\\\__\////\\\////__\////\\\//_____/\\\____\/\\\______/\\\/////\\\_\/\\\//////__                
     _\/\\\_______\/\\\__/\\\__\//\\\____\/\\\_________\/\\\______\/\\\____\/\\\_____/\\\\\\\\\\\__\/\\\\\\\\\\_               
      _\/\\\_______/\\\__\//\\\__/\\\_____\/\\\_/\\_____\/\\\______\/\\\____\/\\\____\//\\///////___\////////\\\_              
       _\/\\\\\\\\\\\\/____\///\\\\\/______\//\\\\\______\/\\\______\/\\\__/\\\\\\\\\__\//\\\\\\\\\\__/\\\\\\\\\\_             
        _\////////////________\/////_________\/////_______\///_______\///__\/////////____\//////////__\//////////__            

```
This is just a collection of my dotfiles. I manage them all with [GNU stow](https://www.archlinux.org/packages/community/any/stow/)

There is a setup script in the repo. **Do not use it**. There are like nine lines. It isn't *even* close to finished. If you want to finish it for me, go ahead and open a PR and I'll merge after review! 😁

# Setup

* **distro:** [arch](archlinux.org)
* **GTK theme:** [wal](https://github.com/dylanaraps/pywal/)
* **icon theme:** [sardi-mono](https://aur.archlinux.org/packages/sardi-icons/) 
* **terminal:** [kitty](https://sw.kovidgoyal.net/kitty/)
* **shell:** [zsh](https://www.zsh.org/) with [oh-my-zsh](https://github.com/robbyrussell/oh-my-zsh)
* **text editor:** [neovim](https://neovim.io/)
* **bar:** [polybar](https://github.com/jaagr/polybar)
* **fonts:** [Fira Code](https://github.com/tonsky/FiraCode/) for polybar, terminal, and rofi and [nerd fonts](https://github.com/ryanoasis/nerd-fonts) for polybar icons
* **wm:** [i3-gaps](https://github.com/Airblader/i3)
* **launcher:** [rofi](https://github.com/DaveDavenport/rofi)
* **lockscreen:** [i3lock-fancy](https://github.com/meskarune/i3lock-fancy)
* **screenshot tool:** [flameshot](https://github.com/lupoDharkael/flameshot)
* **notification daemon:** [dunst](https://dunst-project.org/)
* **file manager:** [ranger](https://github.com/ranger/ranger/)

# Notes
* Credit to my good friend, [Ceda](https://t.me/ceda_ei), for help with the wallpaper and theming script.
* Credit to [jibreil](https://github.com/jibreil/dotfiles) for some of his `/bin` scripts

# Dr. Smith, a custom font

I have developed my own font that is based on my handwriting with a pen. It exceeds the requirements to be an Adobe Western Pro font; it has over 400 characters (not just the English alphabet and punctuation 😉) as well as many ligatures so it simulates my handwriting as closely as possible without me following you around and writing on your computer screen. If you don't want to dig into my files, [this](https://gitlab.com/Amolith/dotfiles/blob/master/fonts/.fonts/DrSmith-Regular.otf) will take you to the `.otf`. If you've found something that doesn't work right, *please* open an issue here and I'll get it fixed as soon as I can. I love this font and want it to be the best it can be.

![](screenshots/dr-smith.png)

If you end up using Dr. Smith, please [email](mailto:amolithseregion@protonmail.com) me a screenshot and/or link and, if possible (without ruining the aesthetics), give me credit for my work and link to my [website](http://nixnet.xyz) or [dotfiles](https://git.nixnet.xyz/Amolith/dotfiles)!

# Screenshots

### clean
![](screenshots/1.png)

### vim, bonsai, snerdy, panes
![](screenshots/2.png)

### fullscreen vim with the [Goyo](https://github.com/junegunn/goyo.vim) plugin
![](screenshots/3.png)

### ranger
![](screenshots/4.png)

[Auto-theming i3-gaps on Arch](https://pe.ertu.be/videos/watch/9bab0dd7-13cb-4dbe-a885-e25e70fa50b4)

This is a link to a Peertube video that demonstrates the theming command that Ceda and I wrote. It gives you a random wallpaper and theme based on the wallpaper every time you run it. It also sends a notification telling you the path to the image and whether or not it's in your favourites (`/Pictures/Wallpapers/Favourites`). Once I have the time to write it, I'll add a part that opens a `xenity` window asking if you want to delete it, copy it to your favourites, or do nothing.
