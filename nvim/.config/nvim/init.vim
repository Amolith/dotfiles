let mapleader =","

if ! filereadable(expand('~/.config/nvim/autoload/plug.vim'))
	echo "Downloading junegunn/vim-plug to manage plugins..."
	silent !mkdir -p ~/.config/nvim/autoload/
	silent !curl "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim" > ~/.config/nvim/autoload/plug.vim
endif

call plug#begin()
Plug 'jceb/vim-orgmode'
Plug 'valloric/MatchTagAlways'
Plug 'Townk/vim-autoclose'
Plug 'fszymanski/deoplete-emoji'
Plug 'DougBeney/pickachu'
Plug 'ap/vim-css-color'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }
Plug 'godlygeek/tabular'
Plug 'tpope/vim-surround'
Plug 'adelarsq/vim-matchit'
Plug 'yggdroot/indentline'
Plug 'vim-syntastic/syntastic'
Plug 'bling/vim-bufferline'
Plug 'edkolev/tmuxline.vim'
Plug 'chase/vim-ansible-yaml'
call plug#end()
let g:deoplete#enable_at_startup = 1

" Enable autocompletion:
	set wildmode=longest,list,full
" Disables automatic commenting on newline:
	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
" Some basics
  set conceallevel=2
  set linebreak
  set foldlevelstart=99
  set clipboard=unnamedplus
  set listchars=tab:⇥-,eol:¬
  set expandtab
  set tabstop=2
  set shiftwidth=4
  set scrolloff=12
  set mouse=a
  set foldmethod=indent
  let g:airline_powerline_fonts = 1
  let g:airline_theme='deus'
  let g:indentLine_char = '▏'
  let g:vimtex_compiler_progname = 'nvr'

" Some basics
  	set nocompatible
  	filetype plugin on
	  syntax on
  	set encoding=utf-8
  	set number relativenumber
" Enable autocompletion:
  	set wildmode=longest,list,full
" Disables automatic commenting on newline:
  	autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Automatically deletes all trailing whitespace on save.
  	autocmd BufWritePre * %s/\s\+$//e

" Runs a script that cleans out tex build files whenever I close out of a .tex file.
  	autocmd VimLeave *.tex !texclear %

"""LATEX
   " Word count:
   autocmd FileType tex map <leader>w :w !detex \| wc -w<CR>
   " Code snippets
   autocmd FileType tex inoremap ,fr \begin{frame}<Enter>\frametitle{}<Enter><Enter><++><Enter><Enter>\end{frame}<Enter><Enter><++><Esc>6kf}i
   autocmd FileType tex inoremap ,fi \begin{fitch}<Enter><Enter>\end{fitch}<Enter><Enter><++><Esc>3kA
   autocmd FileType tex inoremap ,exe \begin{exe}<Enter>\ex<Space><Enter>\end{exe}<Enter><Enter><++><Esc>3kA
   autocmd FileType tex inoremap ,em \emph{}<++><Esc>T{i
   autocmd FileType tex inoremap ,bf \textbf{}<++><Esc>T{i
   autocmd FileType tex vnoremap , <ESC>`<i\{<ESC>`>2la}<ESC>?\\{<Enter>a
   autocmd FileType tex inoremap ,it \textit{}<++><Esc>T{i
   autocmd FileType tex inoremap ,ct \textcite{}<++><Esc>T{i
   autocmd FileType tex inoremap ,cp \parencite{}<++><Esc>T{i
   autocmd FileType tex inoremap ,glos {\gll<Space><++><Space>\\<Enter><++><Space>\\<Enter>\trans{``<++>''}}<Esc>2k2bcw
   autocmd FileType tex inoremap ,x \begin{xlist}<Enter>\ex<Space><Enter>\end{xlist}<Esc>kA<Space>
   autocmd FileType tex inoremap ,ol \begin{enumerate}<Enter><Enter>\end{enumerate}<Enter><Enter><++><Esc>3kA\item<Space>
   autocmd FileType tex inoremap ,ul \begin{itemize}<Enter><Enter>\end{itemize}<Enter><Enter><++><Esc>3kA\item<Space>
   autocmd FileType tex inoremap ,li <Enter>\item<Space>
   autocmd FileType tex inoremap ,ref \ref{}<Space><++><Esc>T{i
   autocmd FileType tex inoremap ,tab \begin{tabular}<Enter><++><Enter>\end{tabular}<Enter><Enter><++><Esc>4kA{}<Esc>i
   autocmd FileType tex inoremap ,ot \begin{tableau}<Enter>\inp{<++>}<Tab>\const{<++>}<Tab><++><Enter><++><Enter>\end{tableau}<Enter><Enter><++><Esc>5kA{}<Esc>i
   autocmd FileType tex inoremap ,can \cand{}<Tab><++><Esc>T{i
   autocmd FileType tex inoremap ,con \const{}<Tab><++><Esc>T{i
   autocmd FileType tex inoremap ,v \vio{}<Tab><++><Esc>T{i
   autocmd FileType tex inoremap ,a \href{}{<++>}<Space><++><Esc>2T{i
   autocmd FileType tex inoremap ,sc \textsc{}<Space><++><Esc>T{i
   autocmd FileType tex inoremap ,chap \chapter{}<Enter><Enter><++><Esc>2kf}i
   autocmd FileType tex inoremap ,sec \section{}<Enter><Enter><++><Esc>2kf}i
   autocmd FileType tex inoremap ,ssec \subsection{}<Enter><Enter><++><Esc>2kf}i
   autocmd FileType tex inoremap ,sssec \subsubsection{}<Enter><Enter><++><Esc>2kf}i
   autocmd FileType tex inoremap ,st <Esc>F{i*<Esc>f}i
   autocmd FileType tex inoremap ,beg \begin{DELRN}<Enter><++><Enter>\end{DELRN}<Enter><Enter><++><Esc>4k0fR:MultipleCursorsFind<Space>DELRN<Enter>c
   autocmd FileType tex inoremap ,up <Esc>/usepackage<Enter>o\usepackage{}<Esc>i
   autocmd FileType tex nnoremap ,up /usepackage<Enter>o\usepackage{}<Esc>i
   autocmd FileType tex inoremap ,tt \texttt{}<Space><++><Esc>T{i
   autocmd FileType tex inoremap ,bt {\blindtext}
   autocmd FileType tex inoremap ,nu $\varnothing$
   autocmd FileType tex inoremap ,col \begin{columns}[T]<Enter>\begin{column}{.5\textwidth}<Enter><Enter>\end{column}<Enter>\begin{column}{.5\textwidth}<Enter><++><Enter>\end{column}<Enter>\end{columns}<Esc>5kA
   autocmd FileType tex inoremap ,rn (\ref{})<++><Esc>F}i
   autocmd StdinReadPre * let s:std_in=1
   autocmd VimEnter * nmap ; :

" Filetypes
   autocmd BufRead,BufNewFile *.tex set filetype=tex
